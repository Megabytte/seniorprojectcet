#ifndef __CUSTOM_BOARD_H__
#define __CUSTOM_BOARD_H__

#if 0
#define DOG_COLLAR 1
#else
#define DOG_REMOTE 1
#endif

#include "nrf_gpio.h"

#define AIN_SWITCH_PIN (NRF_GPIO_PIN_MAP(0, 31))
#define RED_LED_PIN (NRF_GPIO_PIN_MAP(0, 26))
#define GREEN_LED_PIN (NRF_GPIO_PIN_MAP(0, 28))
#define BLUE_LED_PIN (NRF_GPIO_PIN_MAP(0, 30))
#define HEADLIGHT_FET_PIN (NRF_GPIO_PIN_MAP(1, 5))
#define LORA_FET_PIN (NRF_GPIO_PIN_MAP(0, 17))
#define GPS_ENABLE_PIN (NRF_GPIO_PIN_MAP(1, 1))
#define SHOCK_FET_PIN (NRF_GPIO_PIN_MAP(1, 13))

#define POWER_GOOD_PIN (NRF_GPIO_PIN_MAP(0, 20))
#define BAT_CRG_STAT1_PIN (NRF_GPIO_PIN_MAP(0, 21))
#define BAT_CRG_STAT2_PIN (NRF_GPIO_PIN_MAP(0, 22))
#define SDA_PIN (NRF_GPIO_PIN_MAP(0, 23))
#define SCL_PIN (NRF_GPIO_PIN_MAP(0, 24))

#define ALRT_PIN (NRF_GPIO_PIN_MAP(1, 6))
#define MISO_PIN (NRF_GPIO_PIN_MAP(1, 7))
#define MOSI_PIN (NRF_GPIO_PIN_MAP(1, 8))
#define SCK_PIN (NRF_GPIO_PIN_MAP(1, 9))
#define LORA_IRQ_0_PIN (NRF_GPIO_PIN_MAP(0, 8))
#define LORA_IRQ_1_PIN (NRF_GPIO_PIN_MAP(0, 11))
#define LORA_IRQ_2_PIN (NRF_GPIO_PIN_MAP(0, 12))
#define LORA_IRQ_3_PIN (NRF_GPIO_PIN_MAP(0, 6))
#define LORA_IRQ_4_PIN (NRF_GPIO_PIN_MAP(0, 7))
#define LORA_IRQ_5_PIN (NRF_GPIO_PIN_MAP(0, 5))
#define LORA_RESET_PIN (NRF_GPIO_PIN_MAP(0, 13))
#define LORA_SS_PIN (NRF_GPIO_PIN_MAP(0, 14))

#endif
