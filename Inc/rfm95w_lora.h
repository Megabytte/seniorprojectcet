#ifndef _RFM95W_LORA_H_
#define _RFM95W_LORA_H_

#include <stdint.h>

#define ADDRESS_RegFifo (0x00)
#define RESET_RegFifo (0x00)

typedef union
{
    struct
    {
        uint8_t Mode: 3;
        uint8_t LowFrequencyModeOn: 1;
        uint8_t : 1;
        uint8_t AccessSharedReg: 2;
        uint8_t LongRangeMode: 1;
    };
    uint8_t raw;
} RegOpMode;
#define ADDRESS_RegOpMode (0x01)
#define RESET_RegOpMode (0x01)

typedef enum
{
    RegOpMode_Mode_SLEEP        = 0x00,
    RegOpMode_Mode_STDBY        = 0x01,
    RegOpMode_Mode_FSTX         = 0x02,
    RegOpMode_Mode_TX           = 0x03,
    RegOpMode_Mode_FSRX         = 0x04,
    RegOpMode_Mode_RXCONTINUOUS = 0x05,
    RegOpMode_Mode_RXSINGLE     = 0x06,
    RegOpMode_Mode_CAD          = 0x07
} RegOpMode_Mode;

#define ADDRESS_RegFrfMsb (0x06)
#define RESET_RegFrfMsb (0x6C)

#define ADDRESS_RegFrfMid (0x07)
#define RESET_RegFrfMid (0x80)

#define ADDRESS_RegFrfLsb (0x08)
#define RESET_RegFrfLsb (0x00)

typedef union
{
    struct
    {
        uint8_t OutputPower: 4;
        uint8_t MaxPower: 3;
        uint8_t PaSelect: 1;
    };
    uint8_t raw;
} RegPaConfig;
#define ADDRESS_RegPaConfig (0x09)
#define RESET_RegPaConfig (0x4F)

typedef union
{
    struct
    {
        uint8_t PaRamp: 4;
        uint8_t : 1;
        uint8_t : 2;
        uint8_t : 1;
    };
    uint8_t raw;
} RegPaRamp;
#define ADDRESS_RegPaRamp (0x0A)
#define RESET_RegPaRamp (0x09)

typedef union
{
    struct
    {
        uint8_t OcpTrim: 5;
        uint8_t OcpOn: 1;
        uint8_t : 2;
    };
    uint8_t raw;
} RegOcp;
#define ADDRESS_RegOcp (0x0B)
#define RESET_RegOcp (0x2B)

typedef union
{
    struct
    {
        uint8_t LnaBoostHf: 2;
        uint8_t : 1;
        uint8_t LnaBoostLf: 2;
        uint8_t LnaGain: 3;
    };
    uint8_t raw;
} RegLna;
#define ADDRESS_RegLna (0x0C)
#define RESET_RegLna (0x20)

#define ADDRESS_RegFifoAddrPtr (0x0D)
#define RESET_RegFifoAddrPtr (0x00)

#define ADDRESS_RegFifoTxBaseAddr (0x0E)
#define RESET_RegFifoTxBaseAddr (0x80)

#define ADDRESS_RegFifoRxBaseAddr (0x0F)
#define RESET_RegFifoRxBaseAddr (0x00)

#define ADDRESS_RegFifoRxCurrentAddr (0x10)
// Reset: n/a

typedef union
{
    struct
    {
        uint8_t CadDetectedMask: 1;
        uint8_t FhssChangeChannelMask: 1;
        uint8_t CadDoneMask: 1;
        uint8_t TxDoneMask: 1;
        uint8_t ValidHeaderMask: 1;
        uint8_t PayloadCrcErrorMask: 1;
        uint8_t RxDoneMask: 1;
        uint8_t RxTimeoutMask: 1;
    };
    uint8_t raw;
} RegIrqFlagsMask;
#define ADDRESS_RegIrqFlagsMask (0x11)
#define RESET_RegIrqFlagsMask (0x00)

typedef union
{
    struct
    {
        uint8_t CadDetected: 1;
        uint8_t FhssChangeChannel: 1;
        uint8_t CadDone: 1;
        uint8_t TxDone: 1;
        uint8_t ValidHeader: 1;
        uint8_t PayloadCrcError: 1;
        uint8_t RxDone: 1;
        uint8_t RxTimeout: 1;
    };
    uint8_t raw;
} RegIrqFlags;
#define ADDRESS_RegIrqFlags (0x12)
#define RESET_RegIrqFlags (0x00)

#define ADDRESS_RegRxNbBytes (0x13)
// Reset: n/a

#define ADDRESS_RegRxHeaderCntValueMsb (0x14)
// Reset: n/a

#define ADDRESS_RegRxHeaderCntValueLsb (0x15)
// Reset: n/a

#define ADDRESS_RegRxPacketCntValueMsb (0x16)
// Reset: n/a

#define ADDRESS_RegRxPacketCntValueLsb (0x17)
// Reset: n/a

typedef union
{
    struct
    {
        uint8_t SignalDetected: 1;
        uint8_t SignalSynchronized: 1;
        uint8_t RxOnGoing: 1;
        uint8_t HeaderInfoValid: 1;
        uint8_t Clear: 1;
        uint8_t RxCodingRate: 3;
    };
    uint8_t raw;
} RegModemStat;
#define ADDRESS_RegModemStat (0x18)
#define RESET_RegModemStat (0x10)

#define ADDRESS_RegPktSnrValue (0x19)
// Reset: n/a

#define ADDRESS_RegPktRssiValue (0x1A)
// Reset: n/a

#define ADDRESS_RegRssiValue (0x1B)
// Reset: n/a

typedef union
{
    struct
    {
        uint8_t FhssPresentChannel: 6;
        uint8_t RxPayloadCrcOn: 1;
        uint8_t PllTimeout: 1;
    };
    uint8_t raw;
} RegHopChannel;
#define ADDRESS_RegHopChannel (0x1C)
// Reset: n/a

typedef union
{
    struct
    {
        uint8_t ImplicitHeaderModeOn: 1;
        uint8_t CodingRate: 3;
        uint8_t Bw: 4;
    };
    uint8_t raw;
} RegModemConfig1;
#define ADDRESS_RegModemConfig1 (0x1D)
#define RESET_RegModemConfig1 (0x72)

typedef union
{
    struct
    {
        uint8_t SymbTimeout: 2;
        uint8_t RxPayloadCrcOn: 1;
        uint8_t TxContinuousMode: 1;
        uint8_t SpreadingFactor: 4;
    };
    uint8_t raw;
} RegModemConfig2;
#define ADDRESS_RegModemConfig2 (0x1E)
#define RESET_RegModemConfig2 (0x70)

#define ADDRESS_RegSymbTimeoutLsb (0x1F)
#define RESET_RegSymbTimeoutLsb (0x64)

#define ADDRESS_RegPreambleMsb (0x20)
#define RESET_RegPreambleMsb (0x00)

#define ADDRESS_RegPreambleLsb (0x21)
#define RESET_RegPreambleLsb (0x08)

#define ADDRESS_RegPayloadLength (0x22)
#define RESET_RegPayloadLength (0x01)

#define ADDRESS_RegMaxPayloadLength (0x23)
#define RESET_RegMaxPayloadLength (0xFF)

#define ADDRESS_RegHopPeriod (0x24)
#define RESET_RegHopPeriod (0x00)

#define ADDRESS_RegFifoRxByteAddr (0x25)

typedef union
{
    struct
    {
        uint8_t : 2;
        uint8_t AgcAutoOn: 1;
        uint8_t MobileNode: 1;
        uint8_t : 4;
    };
    uint8_t raw;
} RegModemConfig3;
#define ADDRESS_RegModemConfig3 (0x26)
#define RESET_RegModemConfig3 (0x00)

typedef union
{
    struct
    {
        uint8_t Dio3Mapping: 2;
        uint8_t Dio2Mapping: 2;
        uint8_t Dio1Mapping: 2;
        uint8_t Dio0Mapping: 2;
    };
    uint8_t raw;
} RegDioMapping1;
#define ADDRESS_RegDioMapping1 (0x40)
#define RESET_RegDioMapping1 (0x00)

typedef union
{
    struct
    {
        uint8_t MapPreambleDetect: 1;
        uint8_t : 3;
        uint8_t Dio5Mapping: 2;
        uint8_t Dio4Mapping: 2;
    };
    uint8_t raw;
} RegDioMapping2;
#define ADDRESS_RegDioMapping2 (0x41)
#define RESET_RegDioMapping2 (0x00)

#define ADDRESS_RegVersion (0x42)
#define RESET_RegVersion (0x11)

typedef union
{
    struct
    {
        uint8_t : 4;
        uint8_t TcxoInputOn: 1;
        uint8_t : 3;
    };
    uint8_t raw;
} RegTcxo;
#define ADDRESS_RegTcxo (0x4B)
#define RESET_RegTcxo (0x09)

typedef union
{
    struct
    {
        uint8_t PaDac: 3;
        uint8_t : 5;
    };
    uint8_t raw;
} RegPaDac;
#define ADDRESS_RegPaDac (0x4D)
#define RESET_RegPaDac (0x84)

#define ADDRESS_RegFormerTemp (0x5B)
// Reset: n/a

typedef union
{
    struct
    {
        uint8_t AgcReferenceLevel: 6;
        uint8_t : 1;
    };
    uint8_t raw;
} RegAgcRef;
#define ADDRESS_RegAgcRef (0x61)
#define RESET_RegAgcRef (0x13)

typedef union
{
    struct
    {
        uint8_t AgcStep1: 5;
        uint8_t : 3;
    };
    uint8_t raw;
} RegAgcThresh1;
#define ADDRESS_RegAgcThresh1 (0x62)
#define RESET_RegAgcThresh1 (0x0E)

typedef union
{
    struct
    {
        uint8_t AgcStep3: 4;
        uint8_t AgcStep2: 4;
    };
    uint8_t raw;
} RegAgcThresh2;
#define ADDRESS_RegAgcThresh2 (0x63)
#define RESET_RegAgcThresh2 (0x5B)

typedef union
{
    struct
    {
        uint8_t AgcStep5: 4;
        uint8_t AgcStep4: 4;
    };
    uint8_t raw;
} RegAgcThresh3;
#define ADDRESS_RegAgcThresh3 (0x64)
#define RESET_RegAgcThresh3 (0xDB)

#endif
