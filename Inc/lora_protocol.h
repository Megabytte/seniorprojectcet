#ifndef _LORA_PROTOCOL_H_
#define _LORA_PROTOCOL_H_

#include "nrf_gpio.h"
#include "custom_board.h"
#include "rfm95w_lora.h"
#include "nrfx_spim.h"

#define CLAMP(value, low, high) ((value) > (high) ? (high) : ((value) < (low) ? (low) : (value)))

#define PACKET_PAYLOAD_SIZE (28)

typedef struct
{
    uint8_t destinationAddress;
    uint8_t sourceAddress;
    union
    {
        struct
        {
            uint16_t dataLength: 5;
            uint16_t sequenceNumber: 4;
            uint16_t channelNumber: 4;
            uint16_t wrUserBit: 1;
            uint16_t isAck: 1;
            uint16_t isLast: 1;
        };
        uint16_t raw;
    };
    uint8_t data[PACKET_PAYLOAD_SIZE];
} Packet;

typedef enum
{
    PROTOCOL_ERROR_NONE      = 0x00,
    PROTOCOL_ERROR_FAILED    = 0x01,
    PROTOCOL_ERROR_NO_SPACE  = 0x02,
    PROTOCOL_ERROR_TOO_BIG   = 0x03,
    PROTOCOL_ERROR_BUSY      = 0x04
} ProtocolError;

extern const char* ProtocolErrorStr[];
extern uint8_t sourceAddress;

// -------------- Function Prototypes --------------
void loraInit(void);

void setMode(RegOpMode_Mode m);
void setFrequency(long lFrequency);
void setTxPower(int level, bool boostPin);
void loraReset(void);
void enterLoraMode(long frequency, int level, bool boostPin);
void setPreambleLength(long length);
void setCrcEnabled(bool enabled);
void setCodingRate4(int denominator);
void setSignalBandwidth(long sbw);
long getSignalBandwidth(void);
int getSpreadingFactor(void);
void setLdoFlag(void);
void setSpreadingFactor(int sf);
void setHeaderMode(bool implicitMode);
int packetRssi(void);
float packetSnr(void);

extern void (*dataReceivedCallback)(uint8_t* data, uint8_t dataLength);

void protocolInit(uint8_t address);
void protocolUpdate(void);

ProtocolError protocolSyncTransmit(uint8_t destination, uint8_t channel, uint8_t msgLength, const uint8_t* msg);
ProtocolError protocolSyncReceive(uint8_t* out_src, uint8_t* out_msgLength, uint8_t* out_28ByteBuffer);

ProtocolError protocolAsyncTransmit(uint8_t destination, uint8_t channel, uint8_t msgLength, const uint8_t* msg);

void protocolIrq(void);

#endif
