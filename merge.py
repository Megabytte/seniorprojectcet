import sys
from cStringIO import StringIO
from intelhex import IntelHex

original = IntelHex(sys.argv[1])
new = IntelHex(sys.argv[2])
original.merge(new, overlap='error')

sio = StringIO()
original.write_hex_file(sio)
hexstr = sio.getvalue()
sio.close()

handle = open(sys.argv[3], "w")
handle.write(hexstr)
handle.close()
