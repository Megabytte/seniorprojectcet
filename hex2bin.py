import sys
from intelhex import IntelHex

hexFile = IntelHex(sys.argv[1])
hexFile.tobinfile(sys.argv[2])
