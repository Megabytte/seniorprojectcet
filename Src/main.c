#include <stdio.h>
#include <stdlib.h>

#include "sdk_config.h"
#include "ble_logic.h"
#include "custom_board.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrfx_coredep.h"
#include "nrf_gpio.h"
#include "nrfx_spim.h"

#include "lora_protocol.h"

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void)
{
    // Initialize timer module.
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for starting application timers.
 */
static void application_timers_start(void)
{

}

static void sleep_mode_enter(void)
{
    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    #ifdef DEBUG_NRF
     (void) sd_power_system_off();
     for(;;);
    #else
      APP_ERROR_CHECK(sd_power_system_off());
    #endif  // DEBUG_NRF
}

/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

static void gpio_init()
{
    nrf_gpio_cfg(RED_LED_PIN, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_S0S1, NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_cfg(GREEN_LED_PIN, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_S0S1, NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_cfg(BLUE_LED_PIN, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_S0S1, NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_cfg(HEADLIGHT_FET_PIN, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_PULLDOWN, NRF_GPIO_PIN_H0H1, NRF_GPIO_PIN_NOSENSE);
   
    nrf_gpio_cfg(GPS_ENABLE_PIN, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_PULLDOWN, NRF_GPIO_PIN_S0S1, NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_cfg(SHOCK_FET_PIN, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_PULLDOWN, NRF_GPIO_PIN_H0H1, NRF_GPIO_PIN_NOSENSE);

    nrf_gpio_pin_write(RED_LED_PIN, 1);
    nrf_gpio_pin_write(GREEN_LED_PIN, 1);
    nrf_gpio_pin_write(BLUE_LED_PIN, 1);
    nrf_gpio_pin_write(HEADLIGHT_FET_PIN, 0);
    nrf_gpio_pin_write(GPS_ENABLE_PIN, 0); 
}

static void i2c_init()
{
}

static void uart_init()
{
}

static void pdn_init()
{
}

static void adc_init()
{
}

static void pwm_init()
{
}

void gpio_loop()
{
    nrf_gpio_pin_write(RED_LED_PIN, 0);
    nrf_gpio_pin_write(GREEN_LED_PIN, 1);
    nrf_gpio_pin_write(BLUE_LED_PIN, 1);
    nrf_gpio_pin_write(HEADLIGHT_FET_PIN, 0);
    NRFX_DELAY_US(1000000);
    nrf_gpio_pin_write(RED_LED_PIN, 1);
    nrf_gpio_pin_write(GREEN_LED_PIN, 0);
    nrf_gpio_pin_write(BLUE_LED_PIN, 1);
    nrf_gpio_pin_write(HEADLIGHT_FET_PIN, 0);
    NRFX_DELAY_US(1000000);
    nrf_gpio_pin_write(RED_LED_PIN, 1);
    nrf_gpio_pin_write(GREEN_LED_PIN, 1);
    nrf_gpio_pin_write(BLUE_LED_PIN, 0);
    nrf_gpio_pin_write(HEADLIGHT_FET_PIN, 0);
    NRFX_DELAY_US(1000000);
    nrf_gpio_pin_write(RED_LED_PIN, 1);
    nrf_gpio_pin_write(GREEN_LED_PIN, 1);
    nrf_gpio_pin_write(BLUE_LED_PIN, 1);
    nrf_gpio_pin_write(HEADLIGHT_FET_PIN, 1);
    NRFX_DELAY_US(1000000);
}

void shock_test()
{
    static bool t = false;
    t=!t;
    NRFX_DELAY_US(50);
    nrf_gpio_pin_write(SHOCK_FET_PIN, t);
}

void main(void) 
{
    /*
        Dog Collar GATT:
            - See BLE_GATT.xml
    */

    // Developer Board Info:
    //     - MAC (Big Endian): 94-54-93 - 24-46-96
    //     - P0.00=XL1 : P0.01=XL2
    //     - Red LED: P0.13 : Green LED: P0.15 : Blue LED: P0.14
    //     - P0.13=LED1(RED) : P0.14=LED2(RED) : P0.15=LED3(GREEN) : P0.16=LED4(GREEN)
    //     - P0.11=Button1 : P0.12=Button2 : P0.24=Button3 : P0.25=Button4
    //     - P0.17=QSPI_CS : P0.19=QSPI_CLK : P0.20=QSPI_DIO0 : P0.21=QSPI_DIO1 : P0.22=QSPI_DIO2 : P0.23=QSPI_DIO3

    // Peripherals I will be using:
    //     - UART0
    //     - SPI0 (in progress)
    //     - I2C1 (Not I2C0!!)
    //     - PDN0
    //     - PWM0 (& 1 & 2?)
    //     - ADC
    
    /*
        Board Pin Out:
            - (AIN0) P0.02: MIC1_OUT
            - P0.05: LoRa IRQ 5
            - P0.06: LoRa IRQ 3
            - P0.07: LoRa IRQ 4
            - P0.08: LoRa IRQ 0
            - P0.11: LoRa IRQ 1
            - P0.12: LoRa IRQ 2
            - P0.13: LoRa RESET
            - P0.14: LoRa SS
            - P0.15: GPS TP
            - P0.16: GPS RESET
            - P0.17: LoRa FET
            - P0.19: Speaker
            - P0.20: Power Good
            - P0.21: Battery Charge Status 1
            - P0.22: Battery Charge Status 2
            - P0.23: SDA
            - P0.24: SCL
            - P0.25: MIC2_CLK
            - P0.26: LED_R
            - P0.27: MIC2_DATA
            - P0.28: LED_G
            - P0.30: LED_B
            - (AIN7) P0.31: Switch
            - P1.01: GPS_EN
            - P1.02: GPS UART TX
            - P1.03: GPS UART RX
            - P1.04: GPS IRQ
            - P1.05: Headlights FET
            - P1.06: ALRT
            - P1.07: MISO
            - P1.08: MOSI
            - P1.09: SCK
            - P1.13: Shock FET
    */

    // Initialize.
    log_init();
    //timers_init();

    gpio_init();
    loraInit();
//    i2c_init();
//    uart_init();
//    pdn_init();
//    adc_init();
//    pwm_init();

    //ble_stack_init();
    //gap_params_init();
    //gatt_init();
    //advertising_init();
    //services_init();
    //conn_params_init();

    // Start execution.
    NRF_LOG_INFO("Train Em Up Collar firmware started.");

    //application_timers_start();
    //advertising_start();
    
    for (;;) 
    {
        //if (!NRF_LOG_PROCESS()) { power_manage(); }
        
        NRF_LOG_PROCESS();

        protocolUpdate();

        //gpio_loop();
        //shock_test();
    }
}
