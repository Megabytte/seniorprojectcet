#include "lora_protocol.h"
#include "ring_buffer.h"
#include <string.h>

#define TX_PACKET_BUFFER_SIZE (4)
#define RX_PACKET_BUFFER_SIZE (4)

typedef enum
{
    STATE_IDLE,
    STATE_TX_SYNC,
    STATE_RX_SYNC,
    STATE_TX_ASYNC,
    STATE_RX_ASYNC
} ProtocolState;

const char* ProtocolErrorStr[] =
{
    "PROTOCOL_ERROR_NONE",
    "PROTOCOL_ERROR_FAILED",
    "PROTOCOL_ERROR_NO_SPACE",
    "PROTOCOL_ERROR_TOO_BIG",
    "PROTOCOL_ERROR_BUSY"
};

// Callback
void (*dataReceivedCallback)(uint8_t* data, uint8_t dataLength) = NULL;

// ----------------------- Protocol Layer -----------------------
ProtocolState state = STATE_IDLE;
uint8_t sourceAddress = 0xFF;
bool pending = false;
uint8_t txSequenceNumber = 0, rxSequenceNumber = 0;
bool implicitHeaderMode = false;
generic_ring_buffer txPacketBuffer, rxPacketBuffer;
Packet _txPacketBuffer[TX_PACKET_BUFFER_SIZE], _rxPacketBuffer[RX_PACKET_BUFFER_SIZE];
// --------------------------------------------------------------

// --------------------- Function Prototypes --------------------
void spiTransfer(uint8_t* tx, uint8_t* rx, uint16_t length);
uint8_t spiRead(uint8_t reg);
void spiWrite(uint8_t reg, uint8_t val);
void writePacket(Packet* packet);
void setIrqEnabled(bool enabled);
void setIrqFunction(bool isTx);
// --------------------------------------------------------------

static const nrfx_spim_t spi = NRFX_SPIM_INSTANCE(0);  /**< SPI instance. */

void hardwareInit(void);

void loraInit()
{
    hardwareInit();
    
    #if DOG_REMOTE
        protocolInit(0x12);
    #else
        protocolInit(0x56);
    #endif
}

void hardwareInit()
{
    nrf_gpio_cfg(LORA_FET_PIN, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_PULLDOWN, NRF_GPIO_PIN_S0S1, NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_cfg(LORA_RESET_PIN, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_PULLDOWN, NRF_GPIO_PIN_S0S1, NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_cfg(LORA_SS_PIN, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_PULLDOWN, NRF_GPIO_PIN_S0S1, NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_cfg(LORA_IRQ_0_PIN, NRF_GPIO_PIN_DIR_INPUT, NRF_GPIO_PIN_INPUT_CONNECT, NRF_GPIO_PIN_PULLDOWN, NRF_GPIO_PIN_S0S1, NRF_GPIO_PIN_NOSENSE);
    
    nrf_gpio_pin_write(LORA_FET_PIN, 1);
    nrf_gpio_pin_write(LORA_RESET_PIN, 1);
    nrf_gpio_pin_write(LORA_SS_PIN, 1);

    nrfx_spim_config_t spi_config = NRFX_SPIM_DEFAULT_CONFIG;
    spi_config.frequency      = SPIM_FREQUENCY_FREQUENCY_K500;
    spi_config.ss_pin         = NRFX_SPIM_PIN_NOT_USED;
    spi_config.miso_pin       = MISO_PIN;
    spi_config.mosi_pin       = MOSI_PIN;
    spi_config.sck_pin        = SCK_PIN;
    spi_config.ss_active_high = false;
    
    NRFX_ASSERT(nrfx_spim_init(&spi, &spi_config, NULL, NULL) == NRFX_SUCCESS);
    // ^ third argument is event handler, and if it's Null then SPI runs in sync mode
}

void spiTransfer(uint8_t* tx, uint8_t* rx, uint16_t length)
{
    nrfx_spim_xfer_desc_t xfer_desc;
    xfer_desc.p_tx_buffer = tx;
    xfer_desc.p_rx_buffer = rx;
    xfer_desc.tx_length = length;
    xfer_desc.rx_length = length;
    nrf_gpio_pin_write(LORA_SS_PIN, 0);
    nrfx_spim_xfer(&spi, &xfer_desc, 0);
    nrf_gpio_pin_write(LORA_SS_PIN, 1);
}

uint8_t spiRead(uint8_t reg)
{
    uint8_t tx[2];
    uint8_t rx[2];
    tx[0] = 0x7F & reg;
    tx[1] = 0x00;
    spiTransfer(tx, rx, 2);
    return rx[1];
}

void spiWrite(uint8_t reg, uint8_t val)
{
    uint8_t tx[2];
    uint8_t rx[2];
    tx[0] = 0x80 | reg;
    tx[1] = val;
    spiTransfer(tx, rx, 2);
}

void setIrqEnabled(bool enabled)
{
    if(enabled)
    {
        //attachInterrupt(digitalPinToInterrupt(_IRQ), protocol_irq, RISING);
    }
    else
    {
        //detachInterrupt(digitalPinToInterrupt(_IRQ));
    }
}

void setIrqFunction(bool isTx)
{
    RegDioMapping1 mapping;
    mapping.raw = 0;
    mapping.Dio0Mapping = isTx;
    spiWrite(ADDRESS_RegDioMapping1, mapping.raw);
}

void protocolInit(uint8_t address)
{
    sourceAddress = address;

    memset(_txPacketBuffer, 0, sizeof(Packet) * TX_PACKET_BUFFER_SIZE);
    memset(_rxPacketBuffer, 0, sizeof(Packet) * RX_PACKET_BUFFER_SIZE);

    generic_ring_buffer_init(&txPacketBuffer, _txPacketBuffer, sizeof(Packet), TX_PACKET_BUFFER_SIZE);
    generic_ring_buffer_init(&rxPacketBuffer, _rxPacketBuffer, sizeof(Packet), RX_PACKET_BUFFER_SIZE);

    enterLoraMode(915000000, 17, true);
    setIrqFunction(false);

    setIrqEnabled(true);

    protocolSyncTransmit(0xFF, 0, 0, NULL);
}

void protocolUpdate()
{
    //noInterrupts();
    if(state == STATE_TX_ASYNC && txPacketBuffer.count > 0 && !pending)
    {
        pending = true;
        
        Packet packet;
        generic_ring_buffer_get(&txPacketBuffer, &packet);
        
        setMode(RegOpMode_Mode_STDBY); // put into standby so we can load the buffer
        setHeaderMode(false); // explicit header mode
    
        // reset FIFO address and payload length
        spiWrite(ADDRESS_RegFifoAddrPtr, spiRead(ADDRESS_RegFifoTxBaseAddr)); // reset fifo tx address to its base
        spiWrite(ADDRESS_RegPayloadLength, 0); // set current payload length to 0

        writePacket(&packet);
        spiWrite(ADDRESS_RegPayloadLength, 4 + packet.dataLength);

        setIrqFunction(true);

        printf("Sending...\n");
        
        // put in TX mode
        setMode(RegOpMode_Mode_TX);
    }
    //interrupts();
}

// TX State Machine
// 1.) Mode Request STAND-BY
// 2.) Tx Init
// 3.) Write Data FIFO
// 4.) Mode Request TX
// 5.) Wait for IRQ TxDone
// 6.) Automatic Mode Change to STAND-BY
// 7.) New TX? Yes: Goto Step 3, No: Goto Step 8
// 8.) New Mode Request
// Transmit FIFO Filling
// 1.) Set FifoPtrAddr to FifoTxPtrBase
// 2.) Write PayloadLength bytes to the FIFO (RegFifo)
ProtocolError protocolSyncTransmit(uint8_t destination, uint8_t channel, uint8_t msgLength, const uint8_t* msg)
{
    #define DEFER(x) state = STATE_IDLE; setIrqEnabled(true); return (x)
    //Serial.println("Enter Normal");
    setIrqEnabled(false);
    //Serial.println("Enter Normal2");

    if(state != STATE_IDLE) { setIrqEnabled(true); return PROTOCOL_ERROR_BUSY; }

    state = STATE_TX_SYNC;
    uint8_t txSequenceNumber_bk = txSequenceNumber;
    
    Packet packet;
    //                              28 * 4
    if(msgLength > PACKET_PAYLOAD_SIZE * TX_PACKET_BUFFER_SIZE) { DEFER(PROTOCOL_ERROR_TOO_BIG); }
    
    // packetPayloadSize * (total_size - current_count) = 28 * (4 - current_count)
    if(msgLength > PACKET_PAYLOAD_SIZE * (txPacketBuffer.size - txPacketBuffer.count)) { DEFER(PROTOCOL_ERROR_NO_SPACE); }
    
    int remaining = msgLength;
    while(remaining > 0)
    {
        packet.destinationAddress = destination;
        packet.sourceAddress = sourceAddress;
        
        packet.dataLength = remaining > PACKET_PAYLOAD_SIZE ? PACKET_PAYLOAD_SIZE : remaining;
        packet.isLast = remaining - PACKET_PAYLOAD_SIZE <= 0;
        
        packet.sequenceNumber = txSequenceNumber++;
        packet.channelNumber = channel;
        packet.wrUserBit = 1;
        packet.isAck = 0;
        
        memcpy(packet.data, msg + (msgLength - remaining), packet.dataLength);
        memset(packet.data + packet.dataLength, 0, PACKET_PAYLOAD_SIZE - packet.dataLength);
        
        generic_ring_buffer_put(&txPacketBuffer, &packet, false);
        remaining -= PACKET_PAYLOAD_SIZE;
    }

    while(txPacketBuffer.count > 0)
    {
        generic_ring_buffer_peek(&txPacketBuffer, &packet);

        here:
        setMode(RegOpMode_Mode_STDBY); // put into standby so we can load the buffer
        setHeaderMode(false); // explicit header mode

        // reset FIFO address and payload length
        spiWrite(ADDRESS_RegFifoAddrPtr, spiRead(ADDRESS_RegFifoTxBaseAddr)); // reset fifo tx address to its base
        spiWrite(ADDRESS_RegPayloadLength, 0); // set current payload length to 0

        writePacket(&packet);
        spiWrite(ADDRESS_RegPayloadLength, 4 + packet.dataLength);

        // put in TX mode
        setMode(RegOpMode_Mode_TX);
        
        // wait for TX done
        int attempts = 0;
        RegIrqFlags flags;
        flags.raw = spiRead(ADDRESS_RegIrqFlags);
        while (!flags.TxDone)
        {
            flags.raw = spiRead(ADDRESS_RegIrqFlags);
            attempts++;
            if(attempts > 100) 
            {
                while(txPacketBuffer.count) { generic_ring_buffer_get(&txPacketBuffer, &packet); }
                txSequenceNumber = txSequenceNumber_bk;
                DEFER(PROTOCOL_ERROR_FAILED);
                
                printf("TST: %d\n", flags.raw & 0xFF);
                
                NRFX_DELAY_US(100000);
                
                goto here;
            }
        }
        
        // clear IRQ's
        flags.raw = 0;
        flags.TxDone = 1;
        spiWrite(ADDRESS_RegIrqFlags, flags.raw);
        
        generic_ring_buffer_get(&txPacketBuffer, &packet);
    }
   //Serial.print("Leave Normal: ");
//    spiWrite(ADDRESS_RegIrqFlags, 0);
//    Serial.println(spiRead(ADDRESS_RegIrqFlags) & 0xFF);
    DEFER(PROTOCOL_ERROR_NONE);
    #undef DEFER
}

ProtocolError protocolSyncReceive(uint8_t* out_src, uint8_t* out_msgLength, uint8_t* out_28ByteBuffer)
{  
    return PROTOCOL_ERROR_FAILED;
    // RX State Machine
    //  1.) Mode Request STAND-BY
    //  2.) Rx Init, If Single: Goto Step 3, If Continuous: Goto Step 10
    //  3.) Mode Request Rx Single
    //  4.) Wait for IRQ, If RxTimeout: Goto Step 5, If RxDone: Goto Step 6
    //  5.) Automatic Mode change to STAND-BY (Rx Complete, exit state machine)
    //  6.) Automatic Mode change to STAND-BY
    //  7.) IRQ PayloadCrcError? Yes: Goto Step 9, No: Goto Step 8
    //  8.) Read Rx Data
    //  9.) New Mode Request (Rx Complete, exit state machine)
    // 10.) Mode Request Rx Continuous
    // 11.) Wait for IRQ, If RxTimeout: Goto Step 11, If RxDone: Goto Step 12
    // 12.) IRQ PayloadCrcError? Yes: Goto Step 11, No: Goto Step 13
    // 13.) Read Rx Data then Goto Step 11

    // RX Single Mode
    // 1.) Set FifoPtrAddr to FifoRxPtrBase
    // 2.) Select the operating mode: RXSINGLE
    // 3.) The ValidHeader IRQ occurs (if in explicit mode).
    // 4.) The RxDone IRQ occurs.
    // 5.) Automatic Mode change to STAND-BY.
    // 6.) Check PayloadCrcError to see if payload is intact.
    // 7.) If valid payload, read FIFO.
    // 8.) If another RX Single is desired, start at step 1.

    // RX Continous Mode
    // 1.) While in SLEEP or STAND-BY mode, select RXCONT mode.
    // 2.) If valid header CRC, RxDone IRQ occurs.
    // 3.) Check PayloadCrcError to see if payload is intact.
    // 4.) If valid payload, read FIFO.
    // 5.) Steps 2-4 repeat until the user changes the mode.

    // RX FIFO Emptying
    // 1.) Ensure that the following are not set: ValidHeader, PayloadCrcError, RxDone and RxTimeout.
    // 2.) If errors, discard FIFO data. Else: Goto step 3.
    // 3.) Read FifoNbRxBytes to get the number of bytes received so far.
    // 4.) Read FifoRxCurrentAddr to get the FIFO current RX position pointer.
    // 5.) Set FifoPtrAddr to FifoRxCurrentAddr which sets the FIFO pointer to the last packet received.
    // 6.) Extract packet by reading the RegFifo address RegNbRxBytes times.
}

ProtocolError protocolAsyncTransmit(uint8_t destination, uint8_t channel, uint8_t msgLength, const uint8_t* msg)
{
    #define DEFER(x) state = STATE_IDLE; /*interrupts();*/ return (x)
    //noInterrupts();

    if(state != STATE_IDLE) { /*interrupts();*/ return PROTOCOL_ERROR_BUSY; }

    state = STATE_TX_ASYNC;
    
    Packet packet;
    //                              28 * 4
    if(msgLength > PACKET_PAYLOAD_SIZE * TX_PACKET_BUFFER_SIZE) { DEFER(PROTOCOL_ERROR_TOO_BIG); }
    
    // packetPayloadSize * (total_size - current_count) = 28 * (4 - current_count)
    if(msgLength > PACKET_PAYLOAD_SIZE * (txPacketBuffer.size - txPacketBuffer.count)) { DEFER(PROTOCOL_ERROR_NO_SPACE); }
    
    int remaining = msgLength;
    while(remaining > 0)
    {
        packet.destinationAddress = destination;
        packet.sourceAddress = sourceAddress;
        
        packet.dataLength = remaining > PACKET_PAYLOAD_SIZE ? PACKET_PAYLOAD_SIZE : remaining;
        packet.isLast = remaining - PACKET_PAYLOAD_SIZE <= 0;
        
        packet.sequenceNumber = txSequenceNumber++;
        packet.channelNumber = channel;
        packet.wrUserBit = 1;
        packet.isAck = 0;
        
        memcpy(packet.data, msg + (msgLength - remaining), packet.dataLength);
        memset(packet.data + packet.dataLength, 0, PACKET_PAYLOAD_SIZE - packet.dataLength);
        
        generic_ring_buffer_put(&txPacketBuffer, &packet, false);
        remaining -= PACKET_PAYLOAD_SIZE;
    }

    //interrupts(); 
    return PROTOCOL_ERROR_NONE;
    #undef DEFER
}

/*
    LoRa Addressing Operation:
        - With each RxDone IRQ, read RegFifoRxByteAddr into a variable called 'start_address'.
        - Upon reception of ValidHeader IRQ, start polling RegFifoRxByteAddr until it starts to increase.
        - Once RegFifoRxByteAddr >= start_address + 4, the 4 byte address is stored in the FIFO.
        - Read these to see if the address matches what we expect.
        - If it does, do nothing and remain in RX Mode.
        - If it does not, we can return to sleep mode to ignore the rest of the packet.
*/

//0xFF:Hello
void protocolIrq()
{
    RegIrqFlags irqFlags;
    irqFlags.raw = spiRead(ADDRESS_RegIrqFlags);

    printf("IRQ: %d\n", irqFlags.raw & 0xFF);
    printf("%d\n", spiRead(ADDRESS_RegIrqFlagsMask) & 0xFF);
    
    // clear IRQ's
    spiWrite(ADDRESS_RegIrqFlags, irqFlags.raw);

    if(state != STATE_TX_ASYNC || state != STATE_RX_ASYNC) { return; }
    
    if(state == STATE_TX_ASYNC && irqFlags.TxDone)
    {
        pending = false;
        printf("SENT\n");
        if(txPacketBuffer.count == 0)
        {
            printf("IDLE\n");
            state = STATE_IDLE;
        }
    }
    else
    {
        if (!irqFlags.PayloadCrcError)
        {            
            // read packet length
            int packetSize = implicitHeaderMode ? spiRead(ADDRESS_RegPayloadLength) : spiRead(ADDRESS_RegRxNbBytes);
            
            // set FIFO address to current RX address
            spiWrite(ADDRESS_RegFifoAddrPtr, spiRead(ADDRESS_RegFifoRxCurrentAddr));

            if(dataReceivedCallback)
            {
                // dataReceivedCallback(rxPacket.data, rxPacket.dataLength);
            }
            
            // reset FIFO address
            spiWrite(ADDRESS_RegFifoAddrPtr, 0);
        }   
    }
}

void writePacket(Packet* packet)
{
    uint8_t* rawData = (uint8_t*)packet;
    spiWrite(ADDRESS_RegFifo, rawData[0]);
    spiWrite(ADDRESS_RegFifo, rawData[1]);
    spiWrite(ADDRESS_RegFifo, rawData[2]);
    spiWrite(ADDRESS_RegFifo, rawData[3]);
    for(uint8_t i = 0; i < packet->dataLength; i++)
    {
        spiWrite(ADDRESS_RegFifo, rawData[i + 4]);
    }
}

void loraReset()
{
    nrf_gpio_pin_write(LORA_RESET_PIN, 0);
    NRFX_DELAY_US(1000);
    nrf_gpio_pin_write(LORA_RESET_PIN, 1);
    NRFX_DELAY_US(8000);

    spiWrite(ADDRESS_RegOpMode, 0x00);
    spiWrite(ADDRESS_RegOpMode, 0x80);
    spiWrite(ADDRESS_RegOpMode, 0x81);
}

void setMode(RegOpMode_Mode m)
{
    RegOpMode mode;
    mode.raw = 0;
    mode.Mode = m;
    spiWrite(ADDRESS_RegOpMode, mode.raw);
}

void setFrequency(long lFrequency)
{
    uint64_t frf = ((uint64_t)lFrequency << 19) / 32000000;
    spiWrite(ADDRESS_RegFrfMsb, (uint8_t)(frf >> 16));
    spiWrite(ADDRESS_RegFrfMid, (uint8_t)(frf >> 8));
    spiWrite(ADDRESS_RegFrfLsb, (uint8_t)(frf >> 0));
}

void setTxPower(int level, bool boostPin)
{
    RegPaConfig config;
    config.raw = 0;
    
    if(boostPin)
    {
        level = CLAMP(level, 2, 17);
        config.PaSelect = 1;
        config.MaxPower = 0;
        config.OutputPower = level - 2;
    }
    else
    {
        level = CLAMP(level, 0, 14);
        config.PaSelect = 0;
        config.MaxPower = 7;
        config.OutputPower = level;
    }

    spiWrite(ADDRESS_RegPaConfig, config.raw);
}

void enterLoraMode(long frequency, int level, bool boostPin)
{
    loraReset();
    
    setMode(RegOpMode_Mode_SLEEP);
    
    // set frequency
    setFrequency(frequency);

    // set base addresses
    spiWrite(ADDRESS_RegFifoTxBaseAddr, 0x80);
    spiWrite(ADDRESS_RegFifoRxBaseAddr, 0x00);
    
    // set LNA boost
    RegLna regLna;
    regLna.raw = spiRead(ADDRESS_RegLna);
    regLna.LnaBoostHf = 3;
    spiWrite(ADDRESS_RegLna, regLna.raw);
    
    // set auto AGC
    RegModemConfig3 config3;
    config3.raw = 0;
    config3.AgcAutoOn = 1;
    spiWrite(ADDRESS_RegModemConfig3, config3.raw);

    setTxPower(level, boostPin);

    setCodingRate4(8);
    setSpreadingFactor(12);
    setSignalBandwidth(125000);
    setCrcEnabled(true);
    setPreambleLength(12);
    
    setMode(RegOpMode_Mode_STDBY);
}

void setPreambleLength(long length)
{
    spiWrite(ADDRESS_RegPreambleMsb, (uint8_t)(length >> 8));
    spiWrite(ADDRESS_RegPreambleLsb, (uint8_t)(length >> 0));
}

void setCrcEnabled(bool enabled)
{
    RegModemConfig2 config;
    config.raw = spiRead(ADDRESS_RegModemConfig2);
    config.RxPayloadCrcOn = enabled;
    spiWrite(ADDRESS_RegModemConfig2, config.raw);
}

void setCodingRate4(int denominator)
{
    RegModemConfig1 config;
    config.raw = spiRead(ADDRESS_RegModemConfig1);
    config.CodingRate = CLAMP(denominator, 5, 8) - 4;
    spiWrite(ADDRESS_RegModemConfig1, config.raw);
}

void setSignalBandwidth(long sbw)
{
    RegModemConfig1 config;
    config.raw = spiRead(ADDRESS_RegModemConfig1);
    
    if(sbw <= 7800)            { config.Bw = 0; } 
    else if(sbw <= 10400)      { config.Bw = 1; } 
    else if(sbw <= 15600)      { config.Bw = 2; } 
    else if(sbw <= 20800)      { config.Bw = 3; } 
    else if(sbw <= 31250)      { config.Bw = 4; } 
    else if(sbw <= 41700)      { config.Bw = 5; } 
    else if(sbw <= 62500)      { config.Bw = 6; } 
    else if(sbw <= 125000)     { config.Bw = 7; } 
    else if(sbw <= 250000)     { config.Bw = 8; } 
    else /*if(sbw <= 500000)*/ { config.Bw = 9; }

    spiWrite(ADDRESS_RegModemConfig1, config.raw);
    setLdoFlag();
}

long getSignalBandwidth()
{
    RegModemConfig1 config;
    config.raw = spiRead(ADDRESS_RegModemConfig1);
    switch (config.Bw) 
    {
        case 0:  return 7800;
        case 1:  return 10400;
        case 2:  return 15600;
        case 3:  return 20800;
        case 4:  return 31250;
        case 5:  return 41700;
        case 6:  return 62500;
        case 7:  return 125000;
        case 8:  return 250000;
        case 9:  return 500000;
        default: return 0;
    }
}

int getSpreadingFactor()
{
    RegModemConfig2 config;
    config.raw = spiRead(ADDRESS_RegModemConfig2);
    return config.SpreadingFactor;
}

void setLdoFlag()
{
    // Section 4.1.1.5
    long symbolDuration = 1000 / (getSignalBandwidth() / (1L << getSpreadingFactor()));
    
    // Section 4.1.1.6
    bool ldoOn = symbolDuration > 16;

    RegModemConfig3 config;
    config.raw = spiRead(ADDRESS_RegModemConfig3);
    config.AgcAutoOn = ldoOn;
    spiWrite(ADDRESS_RegModemConfig3, config.raw);
}

void setSpreadingFactor(int sf)
{
    sf = CLAMP(sf, 6, 12);

    if(sf == 6)
    {
        // From Datasheet
        spiWrite(0x31, (spiRead(0x31) & 0xF8) | 0x05);
        spiWrite(0x37, 0x0C);
    } 
    else
    {
        // The code below is from a LoRa library for this chip, but seems to make no sense in LoRa mode...
        spiWrite(0x31, 0xC3);
        spiWrite(0x37, 0x0A);
    }

    RegModemConfig2 config;
    config.raw = spiRead(ADDRESS_RegModemConfig2);
    config.SpreadingFactor = sf;
    spiWrite(ADDRESS_RegModemConfig2, config.raw);
    
    setLdoFlag();
}

void setHeaderMode(bool implicitMode)
{
    implicitHeaderMode = implicitMode;
    RegModemConfig1 config;
    config.raw = spiRead(ADDRESS_RegModemConfig1);
    config.ImplicitHeaderModeOn = implicitMode;
    spiWrite(ADDRESS_RegModemConfig1, config.raw);
}

int packetRssi()
{
    return spiRead(ADDRESS_RegPktRssiValue) - 157;
}

float packetSnr()
{
    return ((int8_t)spiRead(ADDRESS_RegPktSnrValue)) * 0.25f;
}