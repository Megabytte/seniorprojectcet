#include "ble_logic.h"

NRF_BLE_GATT_DEF(m_gatt);                                                       /**< GATT module instance. */
BLE_ADVERTISING_DEF(m_advertising);                                             /**< Advertising module instance. */

uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;                        /**< Handle of the current connection. */

ble_uuid_t m_adv_uuids[] =                                               /**< Universally unique service identifiers. */
{
    {BLE_UUID_DEVICE_INFORMATION_SERVICE, BLE_UUID_TYPE_BLE}
};

/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_UNKNOWN);
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the GATT module.
 */
void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing services that will be used by the application.
 */
void services_init(void)
{
    
}

/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    ret_code_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}

/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for initializing the Connection Parameters module.
 */
void conn_params_init(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    ret_code_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            NRF_LOG_INFO("Fast advertising.");
            break;
        case BLE_ADV_EVT_IDLE:
            NRF_LOG_INFO("Advertising idle.");
            //sleep_mode_enter();
            break;
        default:
            NRF_LOG_INFO("on_adv_evt default.");
            break;
    }
}

/*

*/

/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t err_code = NRF_SUCCESS;

    ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected.");
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            break;

        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected.");
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE:
        {
            NRF_LOG_INFO("Connection interval updated: 0x%x, 0x%x.",
                p_gap_evt->params.conn_param_update.conn_params.min_conn_interval,
                p_gap_evt->params.conn_param_update.conn_params.max_conn_interval);
        } break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
        {
           // Accept parameters requested by the peer.
           ble_gap_conn_params_t params;
           params = p_gap_evt->params.conn_param_update_request.conn_params;
           err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle, &params);
           APP_ERROR_CHECK(err_code);

           NRF_LOG_INFO("Connection interval updated (upon request): 0x%x, 0x%x.",
               p_gap_evt->params.conn_param_update_request.conn_params.min_conn_interval,
               p_gap_evt->params.conn_param_update_request.conn_params.max_conn_interval);
        } break;

        case BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST:
        {
            ble_gap_data_length_params_t dl_params;

            // Clearing the struct will effectively set members to @ref BLE_GAP_DATA_LENGTH_AUTO.
            memset(&dl_params, 0, sizeof(ble_gap_data_length_params_t));
            err_code = sd_ble_gap_data_length_update(p_ble_evt->evt.gap_evt.conn_handle, &dl_params, NULL);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
        {
           NRF_LOG_DEBUG("BLE_GATTS_EVT_SYS_ATTR_MISSING");
           err_code = sd_ble_gatts_sys_attr_set(p_gap_evt->conn_handle, NULL, 0, 0);
           APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GAP_EVT_PHY_UPDATE:
        {
            ble_gap_evt_phy_update_t const * p_phy_evt = &p_ble_evt->evt.gap_evt.params.phy_update;

            if (p_phy_evt->status == BLE_HCI_STATUS_CODE_LMP_ERROR_TRANSACTION_COLLISION)
            {
                // Ignore LL collisions.
                NRF_LOG_DEBUG("LL transaction collision during PHY update.");
                break;
            }

            ble_gap_phys_t phys = { 0 };
            phys.tx_phys = p_phy_evt->tx_phy;
            phys.rx_phys = p_phy_evt->rx_phy;

        } break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            NRF_LOG_DEBUG("BLE_GAP_EVT_SEC_PARAMS_REQUEST");
            break;
        case BLE_GAP_EVT_SEC_INFO_REQUEST:
            NRF_LOG_DEBUG("BLE_GAP_EVT_SEC_INFO_REQUEST");
            break;
        case BLE_GAP_EVT_PASSKEY_DISPLAY:
            NRF_LOG_DEBUG("BLE_GAP_EVT_PASSKEY_DISPLAY");
            break;
        case BLE_GAP_EVT_KEY_PRESSED:
            NRF_LOG_DEBUG("BLE_GAP_EVT_KEY_PRESSED");
            break;
        case BLE_GAP_EVT_AUTH_KEY_REQUEST:
            NRF_LOG_DEBUG("BLE_GAP_EVT_AUTH_KEY_REQUEST");
            break;
        case BLE_GAP_EVT_LESC_DHKEY_REQUEST:
            NRF_LOG_DEBUG("BLE_GAP_EVT_LESC_DHKEY_REQUEST");
            break;
        case BLE_GAP_EVT_AUTH_STATUS:
            NRF_LOG_DEBUG("BLE_GAP_EVT_AUTH_STATUS");
            break;
        case BLE_GAP_EVT_CONN_SEC_UPDATE:
            NRF_LOG_DEBUG("BLE_GAP_EVT_CONN_SEC_UPDATE");
            break;
        case BLE_GAP_EVT_TIMEOUT:
            NRF_LOG_DEBUG("BLE_GAP_EVT_TIMEOUT");
            break;
        case BLE_GAP_EVT_RSSI_CHANGED:
            NRF_LOG_DEBUG("BLE_GAP_EVT_RSSI_CHANGED");
            break;
        case BLE_GAP_EVT_ADV_REPORT:
            NRF_LOG_DEBUG("BLE_GAP_EVT_ADV_REPORT");
            break;
        case BLE_GAP_EVT_SEC_REQUEST:
            NRF_LOG_DEBUG("BLE_GAP_EVT_SEC_REQUEST");
            break;
        case BLE_GAP_EVT_SCAN_REQ_REPORT:
            NRF_LOG_DEBUG("BLE_GAP_EVT_SCAN_REQ_REPORT");
            break;
        case BLE_GAP_EVT_DATA_LENGTH_UPDATE:
            NRF_LOG_DEBUG("BLE_GAP_EVT_DATA_LENGTH_UPDATE");
            break;
        case BLE_GAP_EVT_QOS_CHANNEL_SURVEY_REPORT:
            NRF_LOG_DEBUG("BLE_GAP_EVT_QOS_CHANNEL_SURVEY_REPORT");
            break;
        case BLE_GAP_EVT_ADV_SET_TERMINATED:
            NRF_LOG_DEBUG("BLE_GAP_EVT_ADV_SET_TERMINATED");
            ble_gap_evt_adv_set_terminated_t a;
            // hmm
            break;
        case BLE_GATTC_EVT_PRIM_SRVC_DISC_RSP:
            NRF_LOG_DEBUG("BLE_GATTC_EVT_PRIM_SRVC_DISC_RSP");
            break;
        case BLE_GATTC_EVT_REL_DISC_RSP:
            NRF_LOG_DEBUG("BLE_GATTC_EVT_REL_DISC_RSP");
            break;
        case BLE_GATTC_EVT_CHAR_DISC_RSP:
            NRF_LOG_DEBUG("BLE_GATTC_EVT_CHAR_DISC_RSP");
            break;
        case BLE_GATTC_EVT_DESC_DISC_RSP:
            NRF_LOG_DEBUG("BLE_GATTC_EVT_DESC_DISC_RSP");
            break;
        case BLE_GATTC_EVT_ATTR_INFO_DISC_RSP:
            NRF_LOG_DEBUG("BLE_GATTC_EVT_ATTR_INFO_DISC_RSP");
            break;
        case BLE_GATTC_EVT_CHAR_VAL_BY_UUID_READ_RSP:
            NRF_LOG_DEBUG("BLE_GATTC_EVT_CHAR_VAL_BY_UUID_READ_RSP");
            break;
        case BLE_GATTC_EVT_READ_RSP:
            NRF_LOG_DEBUG("BLE_GATTC_EVT_READ_RSP");
            break;
        case BLE_GATTC_EVT_CHAR_VALS_READ_RSP:
            NRF_LOG_DEBUG("BLE_GATTC_EVT_CHAR_VALS_READ_RSP");
            break;
        case BLE_GATTC_EVT_WRITE_RSP:
            NRF_LOG_DEBUG("BLE_GATTC_EVT_WRITE_RSP");
            break;
        case BLE_GATTC_EVT_HVX:
            NRF_LOG_DEBUG("BLE_GATTC_EVT_HVX");
            break;
        case BLE_GATTC_EVT_EXCHANGE_MTU_RSP:
            NRF_LOG_DEBUG("BLE_GATTC_EVT_EXCHANGE_MTU_RSP");
            break;
        case BLE_GATTC_EVT_WRITE_CMD_TX_COMPLETE:
            NRF_LOG_DEBUG("BLE_GATTC_EVT_WRITE_CMD_TX_COMPLETE");
            break;

        default:
            // No implementation needed.
            NRF_LOG_DEBUG("ble_evt_handler default.");
            break;
    }
}

void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t* ram_start = (uint32_t*)0x20002250; // same as RAM_START in linker setup
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, ram_start);
    APP_ERROR_CHECK(err_code);
    // old RAM_START = 0x200032C8
    // old RAM_SIZE = 0x3CD38

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}

/**@brief Function for initializing the Advertising functionality.
 */
void advertising_init(void)
{
    ret_code_t             err_code;
    ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance      = true;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    init.advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids  = m_adv_uuids;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_TIMEOUT_IN_SECONDS;

    init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}

/**@brief Function for the Power manager.
 */
void power_manage(void)
{
    ret_code_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for starting advertising.
 */
void advertising_start()
{
    ret_code_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);

    APP_ERROR_CHECK(err_code);
}
